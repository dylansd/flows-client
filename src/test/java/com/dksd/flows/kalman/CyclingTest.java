package com.dksd.flows.kalman;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

class CyclingTest {

    private double[] x = new double[20];
    private double[] y = new double[20];
    private double[] z = new double[20];

    private double[] mx = new double[20];
    private double[] my = new double[20];
    private double[] mz = new double[20];
    private Random rand = new Random();

    @BeforeEach
    void setUp() {
        //s = 1/2 (u + v) t
        double t = 1.0; //1 sec
        double v = 8.0; //8 m/s
        for (int i = 0; i < 20; i++) {
            x[i] = 4.0 * (double) i;
            y[i] = 0.0;
            z[i] = 0.0;
            mx[i] = x[i] + rand(1.0);
            my[i] = y[i] + rand(1.0);
            mz[i] = z[i] + rand(1.0);
        }
        //kalmanfilter.net/default.aspx
    }

    private double rand(double size) {
        return rand.nextDouble() * size;
    }

    @Test
    void createUser() {
        //Got some great Kalman examples for just implement that...
        //should be good.
        //Add in cubic spline smoother for elevation.
    }

    @Test
    void createFlow() {
    }

    @Test
    void submitFlowMessage() {
    }

    @Test
    void startFlow() {
    }

    @Test
    void pauseFlow() {
    }

    @Test
    void deleteFlow() {
    }

    @Test
    void createClient() {
    }
}