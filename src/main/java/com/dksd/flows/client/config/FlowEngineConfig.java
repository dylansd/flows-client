package com.dksd.flows.client.config;

import com.dksd.flows.common.client.AnnotationClient;
import com.dksd.flows.common.client.DataClient;
import com.dksd.flows.common.client.FlowClient;
import com.dksd.flows.common.client.FlowMessageClient;

public final class FlowEngineConfig {
    private final FlowClient flowClient;
    private final DataClient dataClient;
    private final AnnotationClient annotationClient;
    private final FlowMessageClient flowMessageClient;

    public FlowEngineConfig(FlowClient flowClient,
                            DataClient dataClient,
                            AnnotationClient annotationClient,
                            FlowMessageClient flowMessageClient) {
        this.flowClient = flowClient;
        this.dataClient = dataClient;
        this.annotationClient = annotationClient;
        this.flowMessageClient = flowMessageClient;
    }

    public DataClient getDataClient() {
        return dataClient;
    }

    public FlowClient getFlowClient() {
        return flowClient;
    }

    public AnnotationClient getAnnotationClient() {
        return annotationClient;
    }

    public FlowMessageClient getFlowMessageClient() {
        return flowMessageClient;
    }

}
