package com.dksd.flows.client.kalman;

public class KalmanFilter {

    /*
    Variable acceleration this can apply to x, y  or z if wanting to call it that way

    x^n,n= x^n,n−1+α(zn−x^n,n−1)
    x˙^n,n=x˙^n,n−1+β(zn−x^n,n−1Δt)
    x¨^n,n=x¨^n,n−1+γ(zn−x^n,n−10.5Δt2)

     */
    private double calcXn(double xprev, double alpha, double zn) {
        return alpha * (zn - xprev) + xprev;
    }

    private double calcXdotn(double xdotprev, double xprev, double beta, double zn, double deltat) {
        return xdotprev + beta * (zn - xprev) / deltat;
    }

    private double calcXdoubledotn(double xddotprev, double xprev, double gamma, double zn, double deltat) {
        return xddotprev + gamma * (zn - xprev) / (0.5 * deltat * deltat);
    }
}
