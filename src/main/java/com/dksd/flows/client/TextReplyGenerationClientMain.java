package com.dksd.flows.client;

import com.dksd.flows.client.api.FlowAdminApi;
import com.dksd.flows.client.api.FlowApi;
import com.dksd.flows.client.api.ReportNotFoundException;
import com.dksd.flows.common.exceptions.FlowNotCreatedException;
import com.dksd.flows.common.exceptions.FlowNotFoundException;
import com.dksd.flows.common.exceptions.UserNotFoundException;
import com.dksd.flows.common.factories.FlowCreator;
import com.dksd.flows.common.factories.RuleBuilder;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.helper.UUIDProvider;
import com.dksd.flows.common.model.*;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

//had an idea to use landmark points for labelling instead of the full thing.
//is that possible? Then do similarity tests...

/**
 * Task breakdown...
 * I want small tasks that don't take too long like 5 mins.
 * I want all the pre-requisites added to the ticket.
 * I want to record done-ness. If fully done we can archive?
 */
public final class TextReplyGenerationClientMain {
    public static String apiAdminToken = "dummy_token";

    static FlowAdminApi flowAdminApi = new FlowAdminApi.Builder()
            .withServerAddress("http://localhost:8080")
            .withAdminToken(apiAdminToken)
            .build();
    static FlowApi flowApi;

    public static void main(String[] args) throws IOException, UserNotFoundException, FlowNotFoundException, FlowNotCreatedException, InterruptedException, ReportNotFoundException {
        User user = getOrCreateUser("Dylan Scott-Dawkins");
        System.out.println("User retrieved/created: " + user);
        flowApi = new FlowApi.Builder()
                .withServerAddress("http://localhost:8080")
                .withAuthToken(user.getApiKey())
                .build();
        Flow flow = createTextGenerationFlow(user);
        FlowMsgData msg = null;
        for (int i = 0; i < 10; i++) {
            ObjectNode oNode = JsonHelper.createObjectNode();
            oNode.put("body", i + "Hey @Dylan Scott-Dawkins and @Matthew Gordon i encouraged Robert F to set up a meeting to get something short term and longer term to help him with this or in general running queries; i hope one of you or the people i mentioned (it was in an email) could help");

            msg = flowApi.createFlowMsg(flow, oNode);

            System.out.println("Msg Id to report on: " + msg);
            flowApi.submitFlowMessages(flowApi.getUser(flow), flow.getFlowId(), Arrays.asList(msg));
        }
        System.out.println("Sleeping to wait for for messages to settle");
        Thread.sleep(10000);

        ReportPath report = flowApi.getReport(user, msg.getId());

        System.out.println("Report: " + report);
        for (Map.Entry<Long, Tracker> entry : report.getPath().entrySet()) {
            System.out.println(entry.getValue());
        }
        //Simulate response aka method did something.
        //for (FlowMsgData retMsg : retMsgs) {
        //    Annotation anno = createAnnotation(msg);
        //    flowApi.submitAnnotation(flowApi.getUser(flow), flow.getFlowId(), retMsg.getId(), Arrays.asList(anno));
        //}

    }

    private static User getOrCreateUser(String userName) throws IOException {
        Optional<User> optUser = flowAdminApi.getUser(UUIDProvider.provide(userName));
        if (optUser.isPresent()) {
            return optUser.get();
        }
        return flowAdminApi.createUser(userName);
    }

    private static Annotation createAnnotation(FlowMsgData msg) throws JsonProcessingException {
        ObjectNode an = JsonHelper.createWithKeyValue("task_name",
                "finish shed", "task_description", "descriptions");
        return new Annotation(JsonHelper.toJson(an));
    }

    private static Flow buildTextGenerationFlow(User user) throws FlowNotCreatedException {
        return new FlowCreator.Builder()
                .withUser(user.getUserId())
                .withName("Text reply flow")
                .withDescription("Send it some text and it will reply with response options")

                //https://api-inference.huggingface.co/models/
                .beginFunc("first_text_response", "https://api-inference.huggingface.co/models/facebook/blenderbot-400M-distill", "post")//TEXT CONVERSION

                .markAsAStartStone()
                /*.routeIf("first_rule", "second_text_response",
                        new RuleBuilder()
                                .with("generated_text", Operator.CONTAINS, "Robert was a great man")
                                .build())*/
                .endFunc()

                //.beginFunc("second_text_response", "https://localhost:8080/print", "post")
                //.endFunc()

                .build();
    }

    private static Flow createTextGenerationFlow(User user) throws IOException, FlowNotCreatedException {
        Flow flow = buildTextGenerationFlow(user);
        flowApi.createFlow(user, flow);
        return flow;
    }
}
