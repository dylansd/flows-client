package com.dksd.flows.client.functions;

import com.atlassian.jira.rest.client.api.JiraRestClient;
import com.atlassian.jira.rest.client.api.domain.Transition;
import com.atlassian.jira.rest.client.internal.async.AsynchronousJiraRestClientFactory;
import com.dksd.flows.common.function.core.AbstractFlowMemFunction;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;

import java.net.URI;
import java.util.List;
import java.util.logging.Logger;

public class JiraClientFunction extends AbstractFlowMemFunction {

    private final Logger logger = Logger.getLogger(getClass().getName());

    private static Transition getTransitionByName(Iterable<Transition> transitions, String transitionName) {
        for (Transition transition : transitions) {
            if (transition.getName().equals(transitionName)) {
                return transition;
            }
        }
        return null;
    }

    @Override
    public List<Annotation> handleMessage(FlowMsgData flowMsg) {
        try {
            //https://appen.atlassian.net/jira/software/c/projects/CW/boards/217?modal=detail&selectedIssue=CW-8171
            URI jiraUri = URI.create("https://appen.atlassian.net/jira");
            String username = "dylan.scott-dawkins@figure-eight.com";
            String password = "luru&8Ox15hpd$vMbQHrT16%4BdX3%";
            //Jira API token: KcmXLvxWouxd9VpFzYGa0CFA
            JiraRestClient client = new AsynchronousJiraRestClientFactory().createWithBasicHttpAuthentication(jiraUri, username, password);

            //final Issue issue = client.getIssueClient().getIssue("TST-1", pm);

            //System.out.println(issue);

            // now let's vote for it
            /*restClient.getIssueClient().vote(issue.getVotesUri(), pm);

            // now let's watch it
            restClient.getIssueClient().watch(issue.getWatchers().getSelf(), pm);

            // now let's start progress on this issue
            final Iterable<Transition> transitions = restClient.getIssueClient().getTransitions(issue.getTransitionsUri(), pm);
            final Transition startProgressTransition = getTransitionByName(transitions, "Start Progress");
            restClient.getIssueClient().transition(issue.getTransitionsUri(), new TransitionInput(startProgressTransition.getId()), pm);

            // and now let's resolve it as Incomplete
            final Transition resolveIssueTransition = getTransitionByName(transitions, "Resolve Issue");
            Collection<FieldInput> fieldInputs = Arrays.asList(new FieldInput("resolution", "Incomplete"));
            final TransitionInput transitionInput = new TransitionInput(resolveIssueTransition.getId(), fieldInputs, Comment.valueOf("My comment"));
            restClient.getIssueClient().transition(issue.getTransitionsUri(), transitionInput, pm);
*/

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
