package com.dksd.flows.client.functions;

import com.dksd.flows.common.function.core.AbstractFlowMemFunction;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;

import java.util.List;
import java.util.Random;

public class HumanInputFunction extends AbstractFlowMemFunction {

    private Random rand = new Random();

    @Override
    public List<Annotation> handleMessage(FlowMsgData flowMsg) {
        System.out.println("Human input function" + flowMsg.getData());
        return Annotation.ofKV(flowMsg,
                "task_name", "build shed mock",
                "description", "build a shed at the bottom of the garden, 10x10");
    }

}
