package com.dksd.flows.client.functions;

import com.dksd.flows.common.function.core.AbstractFlowMemFunction;
import com.dksd.flows.common.helper.AbstractJsonTraverser;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.mxgraph.layout.mxCircleLayout;
import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.util.mxCellRenderer;
import org.jgrapht.Graph;
import org.jgrapht.ext.JGraphXAdapter;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

public final class JsonToImageFunction extends AbstractFlowMemFunction {

    private final java.util.logging.Logger logger = Logger.getLogger(getClass().getName());

    public JsonToImageFunction() {
        //Create image from json
    }

    private static FlowMsgData createFlowMsg() {
        FlowMsgData fm = new FlowMsgData();

        return fm;
    }

    @Override
    public List<Annotation> handleMessage(FlowMsgData flowMsg) {
        JsonHandler jsonTraverser = null;
        try {
            jsonTraverser = new JsonHandler(JsonHelper.getParsedPayload(flowMsg));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        try {
            jsonTraverser.getImage();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //TODO populate location of the image
        return null;
        //return new FlowResult(flowMsg);
    }

    public class JsonHandler extends AbstractJsonTraverser {

        private Graph<JsonNode, DefaultEdge> g = new DefaultDirectedGraph<>(DefaultEdge.class);

        public JsonHandler(JsonNode jsonNode) {
            super(jsonNode);
        }

        @Override
        protected void handleArrayItem(JsonNode jsonNode, JsonNode arrayItem, int depth, int index) {
            g.addVertex(arrayItem);
            g.addEdge(jsonNode, arrayItem);
        }

        @Override
        protected void handleValueNode(JsonNode parent, JsonNode jsonNode, int depth) {
            g.addVertex(jsonNode);
            g.addEdge(jsonNode, jsonNode);
        }

        @Override
        protected void handleObject(JsonNode parent, String key, JsonNode jsonNode, int depth, int index) {
            g.addVertex(jsonNode);
            g.addEdge(parent, jsonNode);
        }

        public File getImage() throws IOException {
            JGraphXAdapter<JsonNode, DefaultEdge> graphAdapter =
                    new JGraphXAdapter<>(g);
            mxIGraphLayout layout = new mxCircleLayout(graphAdapter);
            layout.execute(graphAdapter.getDefaultParent());

            BufferedImage image =
                    mxCellRenderer.createBufferedImage(graphAdapter, null, 2, Color.WHITE, true, null);
            File imgFile = new File("/tmp/graph_for_testing" + System.currentTimeMillis() + ".png");
            ImageIO.write(image, "PNG", imgFile);
            return imgFile;
        }
    }

}
