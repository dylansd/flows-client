package com.dksd.flows.client.functions;

import com.dksd.flows.common.function.core.AbstractFlowMemFunction;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;
import com.dksd.neuralnetwork.Network;
import com.dksd.optimization.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class NeuralPsoFunction extends AbstractFlowMemFunction {

    public static final String INPUTS_FIELD = "inputs";
    public static final String PREDICTIONS_FIELD = "predictions";
    public static final String OUTPUTS_FIELD = "outputs";
    public static final String INFER_OR_TRAINING_SAMPLES_FIELD = "infer_or_training_samples";
    public static final String TRAIN_OR_PREDICT_FIELD = "train_or_predict";
    public static final String TRAIN = "train";
    public static final String WEIGHTS_FIELD = "weights";
    private Random random = new Random();
    private boolean inferOnly = false;

    @Override
    public List<Annotation> handleMessage(FlowMsgData flowMsg) {
        JsonNode payload = null;
        try {
            payload = JsonHelper.parseJson(flowMsg.getData());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        validateRequest(payload);
        List<TrainingSample> trainingSamples = loadTrainingData(payload);
        boolean isTraining = isTraining(payload);

        int inputSize = trainingSamples.get(0).getInputs().length;
        int outputSize = trainingSamples.get(0).getExpectedPrediction().length;
        double[] weights = loadWeights(payload);
        if (weights == null) {
            weights = new double[inputSize * 2 * (inputSize + outputSize)];
        }
        int hidden = weights.length / (inputSize + outputSize);
        Network network = createNetwork(inputSize, hidden, outputSize);

        if (isTraining) {
            Gene gbest = trainWithSwarm(network, trainingSamples);
            weights = copy(gbest);
        }
        List<Annotation> annotations = new ArrayList<>();
        for (int i = 0; i < trainingSamples.size(); i++) {//is this right? seems a lot to be in the loop
            double[] inputs = trainingSamples.get(i).getInputs();
            double[] prediction = network.runNetwork(inputs, weights);
            String predictAnno = createPredictAnno(inputs, prediction, weights);
            annotations.add(new Annotation(predictAnno));
        }
        return annotations;
    }

    private double[] copy(Gene gbest) {
        double[] weights = new double[gbest.size()];
        for (int i = 0; i < gbest.size(); i++) {
            weights[i] = gbest.get(i);
        }
        return weights;
    }

    private String createPredictAnno(double[] inputs, double[] prediction, double[] weights) {
        ObjectNode predict = JsonHelper.createObjectNode();
        predict.set(INPUTS_FIELD, JsonHelper.createDoubleArrayNode(inputs));
        predict.set(PREDICTIONS_FIELD, JsonHelper.createDoubleArrayNode(prediction));
        predict.set(WEIGHTS_FIELD, JsonHelper.createDoubleArrayNode(weights));
        try {
            return JsonHelper.toJson(predict);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Gene trainWithSwarm(Network network, List<TrainingSample> trainingSamples) {
        FitnessFunction ff = createFitnessFunction(network, trainingSamples);

        Swarm swarm = new StandardSwarm(ff, 10, 2000);
        while (true) {
            try {
                if (swarm.step()) {
                    break;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        swarm.printGbest();
        //System.out.println("Gbest value: " + swarm.getGbestFitness());
        try {
            swarm.step();//one more step to see
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return swarm.getGbest();
    }

    private void validateRequest(JsonNode payload) {
        boolean training = isTraining(payload);
        if (training && !areWeightsPresent(payload)) {
            //initialize weights.
        }
    }

    private boolean areWeightsPresent(JsonNode payload) {
        return payload.get(WEIGHTS_FIELD) != null;
    }

    private boolean isTraining(JsonNode payload) {
        if (inferOnly || payload.get(TRAIN_OR_PREDICT_FIELD) == null) {
            return true;//create and train if missing
        }
        return TRAIN.equals(payload.get(TRAIN_OR_PREDICT_FIELD).asText());
    }

    private FitnessFunction createFitnessFunction(Network network,
                                                  List<TrainingSample> trainingSamples) {
        return new FitnessFunction() {
            @Override
            public double calcFitness(Particle particle) {
                double error = 0;
                double[] weights = new double[particle.getGene().size()];
                for (int i = 0; i < particle.getGene().size(); i++) {
                    weights[i] = particle.getGene().get(i);
                }
                for (TrainingSample trainingSample : trainingSamples) {
                    double[] prediction = network.runNetwork(trainingSample.getInputs(), weights);
                    double[] expectedPrediction = trainingSample.getExpectedPrediction();
                    double tot = 0;
                    for (int i = 0; i < expectedPrediction.length; i++) {
                        tot += Math.pow(prediction[i] - expectedPrediction[i], 2);
                    }
                    error += tot;
                }
                return error;
            }

            @Override
            public List<Domain> getDomains() {
                List<Domain> domains = new ArrayList<>(100);
                for (int i = 0; i < network.getWeightLength(); i++) {
                    domains.add(new Domain("w", -1, 1));
                }
                return domains;
            }

            @Override
            public Double getEstimatedMinimum() {
                //We expect zero squared error.
                //return null if not applicable
                return 0.0;
            }
        };
    }

    private Network createNetwork(int inputSize, int hiddenSize, int outputSize) {
        return new Network(inputSize, hiddenSize, outputSize);
    }

    private double[] loadWeights(JsonNode payload) {
        //int size = getInputSize(payload) * getHiddenSize(payload) + getHiddenSize(payload) * getOutputSize(payload);
        double[] weights = null; //new double[size];
        ArrayNode weightsEntries = JsonHelper.getArrayNode(payload, WEIGHTS_FIELD);
        if (weightsEntries == null) {
            return null;
        }
        for (int i = 0; i < weightsEntries.size(); i++) {
            if (weights == null) {
                weights = new double[weightsEntries.size()];
            }
            double value = weightsEntries.get(i).asDouble();
            weights[i] = value;
        }
        return weights;
    }

    private List<TrainingSample> loadTrainingData(JsonNode payload) {
        List<TrainingSample> samples = new ArrayList<>();
        ArrayNode trainEntries = JsonHelper.getArrayNode(payload, INFER_OR_TRAINING_SAMPLES_FIELD);
        if (trainEntries == null) {
            //Set training to false
            inferOnly = true;
            samples.add(new TrainingSample(defaultInputs(), randomizeOutputs()));//just add 1 for now.
            //no need to learn the wrong function...
            return samples;
        }
        for (JsonNode entry : trainEntries) {
            double[] inputs = getTrainInputs(entry);
            double[] predictions = getTrainPredicts(entry);
            samples.add(new TrainingSample(inputs, predictions));
        }
        return samples;
    }

    private double[] randomizeOutputs() {
        double[] outputs = new double[10];//default to 10. dont have to use all downstream but at least avail.
        for (int i = 0; i < outputs.length; i++) {
            outputs[i] = random.nextDouble();
        }
        return outputs;
    }

    private double[] defaultInputs() {
        double[] inputs = new double[3];
        inputs[0] = (double) LocalDate.now().getMonthValue() / 12.0;
        inputs[1] = (double) LocalDate.now().getDayOfWeek().getValue() / 7.0;
        inputs[2] = (double) LocalDateTime.now().getHour() / 23.0;
        return inputs;
    }

    private double[] getTrainInputs(JsonNode entry) {
        double[] inputs = null;
        ArrayNode weightsEntries = JsonHelper.getArrayNode(entry, INPUTS_FIELD);
        for (int i = 0; i < weightsEntries.size(); i++) {
            if (inputs == null) {
                inputs = new double[weightsEntries.size()];
            }
            double value = weightsEntries.get(i).asDouble();
            inputs[i] = value;
        }
        return inputs;
    }

    private double[] getTrainPredicts(JsonNode entry) {
        double[] predictions = null;
        ArrayNode weightsEntries = JsonHelper.getArrayNode(entry, OUTPUTS_FIELD);
        for (int i = 0; i < weightsEntries.size(); i++) {
            if (predictions == null) {
                predictions = new double[weightsEntries.size()];
            }
            double value = weightsEntries.get(i).asDouble();
            predictions[i] = value;
        }
        return predictions;
    }

}
//Going to be a big freakin message.
//so could use big queue to download data into.
//This is awesome though
/*
{
train_or_predict: "train|predict"
input_size: 1,
hidden_size: 1,
output_size: 1,
domains: [{name,start,end}, {...}],
infer_or_training_samples: {   //optional
         inputs: [1,2,3,4],
         predictions: [2,3,4,5]
     },
weights: []
}
 */


/*private List<Domain> getDomainFromMsg(JsonNode payload) {
        List<Domain> domains = new ArrayList<>();
        ArrayNode domainEntries = JsonHelper.getArrayNode(payload, "domains");
        for (JsonNode entry : domainEntries) {
            String name = entry.get("name").asText();
            double start = entry.get("start").asDouble();
            double end = entry.get("end").asDouble();
            domains.add(new Domain(name, start, end));
        }
        return domains;
    }*/