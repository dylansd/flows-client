package com.dksd.flows.client.functions;

import com.dksd.flows.common.function.core.AbstractFlowMemFunction;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.*;

public final class MealSelectorFunction extends AbstractFlowMemFunction {

    private List<String> idsToSelectFrom = new ArrayList<>();

    {
        idsToSelectFrom.add("00bc06c3-1684-4cd1-9a66-f446483ae667");
        idsToSelectFrom.add("655d13ac-08e2-45cd-aff6-81766cc6198f");
        idsToSelectFrom.add("708ab59d-af27-4ef3-95ad-021d8e623ebf");
        idsToSelectFrom.add("9948113a-0ff3-4974-ad06-343808420bea");
        idsToSelectFrom.add("cea1b2c5-0a9b-4eb1-b613-6cd0fb072464");
        idsToSelectFrom.add("0946298d-1aa2-43d7-98dc-073f7a345835");
        idsToSelectFrom.add("67bd9b63-f54e-409b-9afe-0bf6f9d85b23");
        idsToSelectFrom.add("7ca05b02-2574-4bcc-982c-8cf7ec54ebbb");
        idsToSelectFrom.add("9c0f44e9-6fbc-42d0-b137-a984eb721872");
        idsToSelectFrom.add("db279b02-ab8c-473a-be7d-2bb5435c5b39");
        idsToSelectFrom.add("27498117-9767-43c8-9011-8b9636669fbf");
        idsToSelectFrom.add("6a1b3691-f2fa-4d9c-9c38-cce60c44ff5e");
        idsToSelectFrom.add("90eb98c9-b824-4a5c-804d-06136b8b140e");
        idsToSelectFrom.add("9e8aa229-d879-4349-9036-6b7a2eb9090c");
        idsToSelectFrom.add("2876e554-96c0-4230-9c8b-8c6054ae57bb");
        idsToSelectFrom.add("6b403098-e41c-489c-b1c7-2eecd78d6bbe");
        idsToSelectFrom.add("9943011d-6987-448f-966d-300ef2b43cfa");
        idsToSelectFrom.add("c66b9305-af7b-47d1-a3a3-8e9562e76bf5");
    }

    @Override
    public List<Annotation> handleMessage(FlowMsgData flowMsg) {
        try {
            ObjectNode jsonNode = JsonHelper.getParsedPayloadObj(flowMsg);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        ObjectNode weekPlan = JsonHelper.createObjectNode();

        weekPlan.put("tonight", pickMeal());
        weekPlan.put("tomorrow", pickMeal());
        weekPlan.put("The day after tomorrow", pickMeal());

        try {
            return Arrays.asList(new Annotation(
                    JsonHelper.toJson(weekPlan)));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    private String pickMeal() {
        return idsToSelectFrom.get(new Random().nextInt(idsToSelectFrom.size()));
    }

}

