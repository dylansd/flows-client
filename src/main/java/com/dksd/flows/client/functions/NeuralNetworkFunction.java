package com.dksd.flows.client.functions;

import com.dksd.flows.common.function.core.AbstractFlowMemFunction;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;

import java.util.List;
import java.util.logging.Logger;

public final class NeuralNetworkFunction extends AbstractFlowMemFunction {

    private final java.util.logging.Logger logger = Logger.getLogger(getClass().getName());

    public NeuralNetworkFunction() {
        //Create weights from template
        //Setup all the data sources
        //Start a training loop, split training data etc.
        //We need the feedback data too tho...
    }

    @Override
    public List<Annotation> handleMessage(FlowMsgData flowMsg) {
        //Get type of neural network...
        //Support image only now.
        //Load image
        //Output action to take...
        //Add a node etc...
        //Fin
        return null;//new FlowResult(flowMsg);
    }

}
