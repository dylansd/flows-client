package com.dksd.flows.client.functions;

import com.dksd.flows.common.function.core.AbstractFlowMemFunction;
import com.dksd.flows.common.helper.AbstractJsonTraverser;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.util.*;

public class JsonNodeSelectorFunction extends AbstractFlowMemFunction {
    @Override
    public List<Annotation> handleMessage(FlowMsgData flowMsg) {
        JsonNode payload = getPayload(flowMsg);
        JsonNode dataToSelectFrom = payload.get("data_inputs");
        JsonDepthCounter depthCounter = new JsonDepthCounter(dataToSelectFrom);
        List<Annotation> annos = new ArrayList<>();
        List<Double> predictions = getDepthIndexPairs(payload);
        for (int i = 0; i < predictions.size() - 1; i++) {
            int max = depthCounter.getMaxDepth();
            int depth = (int) (max * predictions.get(i));
            int maxIndexAtDepth = depthCounter.getMaxIndexAtDepth(depth);
            int index = (int) (maxIndexAtDepth * predictions.get(i + 1));
            JsonNode node = depthCounter.getNodeAt("" + depth + index);
            annos.add(createAnnotation(flowMsg, node));
        }
        return annos;
    }

    private Annotation createAnnotation(FlowMsgData flowMsg, JsonNode node) {
        try {
            return new Annotation(JsonHelper.toJson(node));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private List<Double> getDepthIndexPairs(JsonNode payload) {
        ArrayNode predArr = (ArrayNode) payload.get("predictions");
        List<Double> dbls = new ArrayList<>();
        for (int i = 0; i < predArr.size(); i++) {
            dbls.add(predArr.get(i).asDouble());
        }
        return dbls;
    }

    private JsonNode getPayload(FlowMsgData flowMsg) {
        try {
            return getParsedPayload(flowMsg);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    private class JsonDepthCounter extends AbstractJsonTraverser {
        private TreeMap<Integer, Integer> maxIndexAtDepth;
        private Map<String, JsonNode> nodeRefs;

        public JsonDepthCounter(JsonNode jsonNode) {
            super(jsonNode);
        }

        @Override
        protected void handleArrayItem(JsonNode parent, JsonNode arrayItem, int depth, int index) {
            setMaxIndex(depth, index);
            setNodeRef("" + depth + index, arrayItem);
        }

        private void setNodeRef(String key, JsonNode node) {
            if (nodeRefs == null) {
                nodeRefs = new HashMap<>();
            }
            nodeRefs.put(key, node);
        }

        @Override
        protected void handleValueNode(JsonNode parent, JsonNode jsonNode, int depth) {
            setMaxIndex(depth, 0);
            setNodeRef("" + depth + "0", jsonNode);
        }

        private void setMaxIndex(int depth, int index) {
            if (maxIndexAtDepth == null) {
                maxIndexAtDepth = new TreeMap<>();
            }
            if (maxIndexAtDepth.get(depth) == null) {
                maxIndexAtDepth.put(depth, index);
                return;
            }
            if (index > maxIndexAtDepth.get(depth)) {
                maxIndexAtDepth.put(depth, index);
            }
        }

        @Override
        protected void handleObject(JsonNode parent, String key, JsonNode jsonNode, int depth, int index) {
            setMaxIndex(depth, index);
            setNodeRef("" + depth + index, jsonNode);
        }

        public int getMaxDepth() {
            return maxIndexAtDepth.lastKey();
        }

        public int getMaxIndexAtDepth(int depth) {
            return maxIndexAtDepth.get(depth);
        }

        public JsonNode getNodeAt(String offset) {
            return nodeRefs.get(offset);
        }
    }
}
