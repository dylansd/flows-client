package com.dksd.flows.client.functions;

import com.dksd.flows.common.function.core.AbstractFlowMemFunction;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class TaskSplitFunction extends AbstractFlowMemFunction {

    @Override
    public List<Annotation> handleMessage(FlowMsgData flowMsg) {
        //Split large item up.
        //tnde.put("task_name", "Office Completion");
        //tnde.put("task_description", "Finish the office, paint steps inside, clean up etc");
        //tnde.put("est_min", 10 * 60);
        try {
            //System.out.println("Seeing msg: " + JsonHelper.toJson(flowMsg));
            return Arrays.asList(createAnno(JsonHelper.getParsedPayloadObj(flowMsg)));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    private Annotation createAnno(ObjectNode payload) throws JsonProcessingException {
        int newEst = payload.get("est_min").asInt() / 2;
        payload.put("est_min", newEst);
        return new Annotation(JsonHelper.toJson(payload));
    }

}
