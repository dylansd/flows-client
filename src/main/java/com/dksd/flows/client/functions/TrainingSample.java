package com.dksd.flows.client.functions;

public class TrainingSample {
    private final double[] inputs;
    private final double[] predicts;

    public TrainingSample(double[] inputs, double[] predicts) {
        this.inputs = inputs;
        this.predicts = predicts;
    }

    public double[] getInputs() {
        return inputs;
    }

    public double[] getExpectedPrediction() {
        return predicts;
    }
}
