package com.dksd.flows.client.functions;

import com.dksd.flows.common.function.core.AbstractFlowMemFunction;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;

import java.util.List;

/*
Goal of this function is to figure out and apply a force to
brake a falling branch with the least spike in tension or force
and one with the lowest overall average velocity...
 */
public class TreeBrakingFunction extends AbstractFlowMemFunction {
    @Override
    public List<Annotation> handleMessage(FlowMsgData flowMsg) {
        //Those x-rings are pretty awesome for a solution.
        //Or zip line which is also pretty sweet.
        //Given inputs:
        //rotation acceleration, prev_rotation_speed, curr_rotation_speed, curr_clamp_pos
        //Be great to build a manual version and record the data.
        //Then you would have some real life stuff.
        //on the other hand simulation can happen before.
        //Best would be a manual descent controller.
        //simulate a falling object. f = ma
        //rope and friction up.
        // f = ma down for a branch...
        // f = ma up for brake
        //There is slippage at the intrface which is a problem...

        return null;
    }

    private double getRotationSpeed() {
        return 0;
    }

    /*
    Algorithm:
    bang bang control?
    what sensors do we need?
    rotation speed lets say...

basically we trying to guess the weight of the branch using only rotation speed

1. start in open position
2. Every 10 ms increase clamping force
3. Did rotation slow?
4. By how much?
5. Based on rotation slow down, calculate forces...
p=mv
rotation should equal speed...



    Lineraly
     */


}
