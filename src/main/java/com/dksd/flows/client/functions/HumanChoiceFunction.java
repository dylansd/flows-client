package com.dksd.flows.client.functions;

import com.dksd.flows.common.function.core.AbstractFlowMemFunction;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;

import java.util.List;
import java.util.Random;

public class HumanChoiceFunction extends AbstractFlowMemFunction {

    private Random rand = new Random();

    @Override
    public List<Annotation> handleMessage(FlowMsgData flowMsg) {
        System.out.println("Make your choice: " + flowMsg.getData());
        return Annotation.ofKV(flowMsg, "choice", "create_task");
    }

}
