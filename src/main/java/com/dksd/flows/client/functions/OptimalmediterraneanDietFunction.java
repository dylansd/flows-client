package com.dksd.flows.client.functions;

import com.dksd.flows.common.function.core.AbstractFlowMemFunction;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Can be step related data that would be fun to get at and scalable.
 * Per function related data even more sense?
 * Instead of a graph we can show the json with clickable text.
 * Boom, just walked up the tree and launched my line in first hit. Damn son.
 * <p>
 * I think I have it .
 * Neural Network selects a depth and a random instance at that depth.
 * Maybe three options that it hasnt seen before. Also feed in the previous selected ones if possible
 */
public final class OptimalmediterraneanDietFunction extends AbstractFlowMemFunction {

    private static final DecimalFormat df2 = new DecimalFormat("#.##");

    public static void main(String[] args) {
        OptimalmediterraneanDietFunction func = new OptimalmediterraneanDietFunction();
        FlowMsgData msg = new FlowMsgData();
        //msg.setUserId(UUID.fromString("fdce9175-5793-4606-bfd4-9c7727a9ebb5"));
        func.handleMessage(msg);
        //So data in the msg? out the message?
      /*
      heres a thought...
      Allow the human to manipulate the flow too.
      So if we had the mapper to reduce the data...
      The user could be selecting the lens and settings that provide different data shapes.
      Wow this could actually be really cool.
      Then the flow solidifies and the data inputs can just flow.
      Start with graphing all the data for a function... or flow? can support either.

       */
    }

    @Override
    public List<Annotation> handleMessage(FlowMsgData flowMsg) {
        //Also this logic is too much code fuck sake.
        String exampleIdsData = "";
    /*   {
           "weekPlan":[
              "cea1b2c5-0a9b-4eb1-b613-6cd0fb072464",
              "6b403098-e41c-489c-b1c7-2eecd78d6bbe",
              "9e8aa229-d879-4349-9036-6b7a2eb9090c",
              "9943011d-6987-448f-966d-300ef2b43cfa",
              "708ab59d-af27-4ef3-95ad-021d8e623ebf"
           ]
        }
        """;*/
        String exampleWeek3IdsData = "";
        /*{
           "weekPlan":[
             "655d13ac-08e2-45cd-aff6-81766cc6198f",
             "a003630e-4f05-4e50-aa8d-df667f5d6fa6"
           ]
        }
        """;*/
        Map<String, Double> foodNameQtyToShop = new HashMap<>(10);
        JsonNode weekPlan = null;
        try {
            weekPlan = JsonHelper.getObjectMapper().readTree(exampleWeek3IdsData).get("weekPlan");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        List<String> names = new ArrayList<>(20);
        for (JsonNode recipeNode : weekPlan) {
            UUID recipeId = UUID.fromString(recipeNode.asText());

            String recipeStr = null;
            //try {
            //String objId, UserId userId, String path
            //TODO should use data source
            //recipeStr = getClientManager().getData(recipeId.toString(), getClientManager().getUser(flowMsg).getUserId(), getPath());
            //} catch (IOException e) {
            //  e.printStackTrace();
            //}
            if (recipeId == null || recipeStr == null) {
                //logger.error("Could not find recipe with id: " + recipeId);
                continue;
            }
            JsonNode recipe = null;
            try {
                recipe = JsonHelper.getObjectMapper().readTree(recipeStr);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            names.add(recipe.get("name").asText());
            for (JsonNode grocery : recipe.get("groceries")) {
                try {
                    addQty(foodNameQtyToShop, grocery);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    /*prettyPrintOutput(names, foodNameQtyToShop);
    FlowResult result = FlowResult.emptyResult(flowMsg);
    FlowMessage msg = new FlowMessage(UUIDProvider.provide(flowMsg), flowMsg);
    try {
      msg.setData(JsonHelper.toJson(foodNameQtyToShop));
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }
    result.addSuccessResult(msg);
    return result;*/
    }

    private String getPath() {
        //TODO
        return null;
    }

    private void prettyPrintOutput(List<String> names, Map<String, Double> foodNameQtyToShop) {
        for (String name : names) {
            System.out.println(name);
        }
        System.out.println();
        //System.out.println(foodNameQtyToShop);
        //Now for the pretty version:
        for (Map.Entry<String, Double> entry : foodNameQtyToShop.entrySet()) {
            double grams = entry.getValue() / 1000.0;
            double ounce = grams * 0.035274;
            double pound = grams * 0.0022046249999752;
            double cups = grams / 201;
            System.out.println(entry.getKey() + ": " + df2.format(grams) + " grams, " + df2.format(ounce) + " ounces, "
                    + df2.format(pound) + " pounds, " + df2.format(cups) + " cups.");
        }
    }

    private void addQty(Map<String, Double> foodNameQtyToShop, JsonNode grocery) throws IOException {
        String name = grocery.get("name").asText();
        Double qty = grocery.get("qty").asDouble();
        String units = grocery.get("unit").asText();

        foodNameQtyToShop.putIfAbsent(name, 0.0);
        foodNameQtyToShop.put(name, foodNameQtyToShop.get(name) + convertToMilligrams(name, qty, units));
    }

    private Double convertToMilligrams(String name, Double qty, String units) {
        if ("OUNCE".equals(units) || "OUNCES".equals(units)) {
            return qty * 28349.5;
        } else if ("TBLSPOON".equals(units) || "TABLESPOON".equals(units) || "TBLSPOONS".equals(units)) {
            return qty * 14786.76484375;
        } else if ("TEASPOON".equals(units)) {
            return qty * 5000.0;
        } else if ("GRAM".equals(units) || "GRAMS".equals(units)) {
            return qty * 1000.0;
        } else if ("PINT".equals(units)) {
            return qty * 473.176475 * 1000;
        } else if ("POUND".equals(units) || "POUNDS".equals(units)) {
            return qty * 453.592 * 1000;
        } else if ("CUP".equals(units) || "CUPS".equals(units)) {
            return qty * 128 * 1000;
        } else if ("CAN".equals(units) || "CANS".equals(units)) {
            return convertToMilligrams(name, 12.0, "OUNCE");
        } else if ("SPRAY".equals(units)) {
            return qty * 1000;
        } else if ("SLICE".equals(units) || "SLICES".equals(units)) {
            return convertToMilligrams(name, 50.0, "GRAM");
        } else if ("WHOLE".equals(units)) {
            if (name.contains("slice")) {
                return qty * 25 * 1000;
            }
            if (name.contains("onion")) {
                return qty * 100 * 1000;
            }
            if (name.contains("garlic")) {
                return qty * 5 * 1000;
            }
            if (name.contains("lemon")) {
                return qty * 100 * 1000;
            }
            if (name.contains("orange")) {
                return qty * 184 * 1000;
            }
            return qty * 100 * 1000;
        }
        throw new IllegalArgumentException("unknown units: " + units);
    }

}