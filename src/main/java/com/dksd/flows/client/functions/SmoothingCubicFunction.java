package com.dksd.flows.client.functions;

import com.dksd.flows.common.function.core.AbstractFlowMemFunction;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;
import umontreal.ssj.functionfit.SmoothingCubicSpline;

import java.util.List;
import java.util.Random;

public class SmoothingCubicFunction extends AbstractFlowMemFunction {

    private static Random rand = new Random();

    public static void main(String[] args) {
        //Just take the last 5-10 points
        //double[] x, double[] y, double rho
        double[] x = new double[10];
        double[] h = new double[10];
        double rho = 0.5; //set to 1 is interpolating spline
        x[0] = 0;
        for (int i = 1; i < 10; i++) {
            x[i] = i;
            double nd = rand.nextDouble();
            if (nd < 0.5) {
                nd = -nd;
            }
            nd = nd * 10;
            h[i] = x[i - 1] + nd;
            System.out.println("Height: " + h[i]);
        }
        SmoothingCubicSpline smoothingCubicSpline = new SmoothingCubicSpline(x, h, rho);
        for (int i = 1; i < 10; i++) {
            double ans = smoothingCubicSpline.evaluate(i);
            System.out.println("" + ans);
        }
    }

    @Override
    public List<Annotation> handleMessage(FlowMsgData flowMsg) {
        //TODO we want to put in a new topic the smoothed altitude.
        System.out.println("Human input function" + flowMsg.getData());
        return Annotation.ofKV(flowMsg,
                "task_name", "build shed mock",
                "description", "build a shed at the bottom of the garden, 10x10");
    }

}
