package com.dksd.flows.client;

import com.dksd.flows.client.api.FlowAdminApi;
import com.dksd.flows.client.api.FlowApi;
import com.dksd.flows.common.exceptions.FlowNotCreatedException;
import com.dksd.flows.common.exceptions.FlowNotFoundException;
import com.dksd.flows.common.exceptions.UserNotFoundException;
import com.dksd.flows.common.factories.FlowCreator;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.model.Flow;
import com.dksd.flows.common.model.User;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Task breakdown...
 * I want small tasks that don't take too long like 5 mins.
 * I want all the pre-requisites added to the ticket.
 * I want to record done-ness. If fully done we can archive?
 */
public final class TestTaskClientMain {

    static FlowAdminApi flowAdminApi = new FlowAdminApi.Builder().withAdminToken(TextReplyGenerationClientMain.apiAdminToken).withServerAddress("http://localhost:8080")
            .build();
    static FlowApi flowApi = new FlowApi.Builder().withServerAddress("http://localhost:8080")
            .build();

    public static void main(String[] args) throws IOException, UserNotFoundException, FlowNotFoundException, FlowNotCreatedException, InterruptedException {
        Flow flow = createTaskBreakdownFlow();
        ObjectNode oNode = JsonHelper.createObjectNode();
        oNode.put("action", "create_task");

        ExecutorService exec = Executors.newFixedThreadPool(100);

        for (int i = 0; i < 100; i++) {
            final int k = i;
            exec.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        FlowMsgData msg = flowApi.createFlowMsg(flow, oNode);
                        List<FlowMsgData> retMsgs = flowApi.submitFlowMessages(flowApi.getUser(flow), flow.getFlowId(), Arrays.asList(msg));

                        for (FlowMsgData retMsg : retMsgs) {
                            Annotation anno = createAnnotation(msg);
                            flowApi.submitAnnotation(flowApi.getUser(flow), flow.getFlowId(), retMsg.getId(), Arrays.asList(anno));
                            System.out.println("Sending annotation " + k + ": " + anno.getAnnoId().toString());
                        }
                    } catch (Exception ep) {
                        ep.printStackTrace();
                    }
                }
            });
        }
        exec.awaitTermination(1, TimeUnit.HOURS);
    }

    private static Annotation createAnnotation(FlowMsgData msg) throws JsonProcessingException {
        ObjectNode an = JsonHelper.createWithKeyValue("task_name",
                "finish shed", "task_description", "descriptions");
        Annotation anno = new Annotation(JsonHelper.toJson(an));
        return anno;
    }

    //Maybe just need a REST endpoint function?
    //Or maybe Kafka since I fucking hate REST.
    //This may be
    private static Flow buildTaskBreakdownFlow(User user) throws FlowNotCreatedException {
        return new FlowCreator.Builder()
                .withUser(user.getUserId())
                .withName("Task creation flow")
                .withDescription("Creates a task and inserts it into the kafka topic")


                .beginFunc("create_task", "https://localhost:8080/human_input", "post")
                //.filterIf("(action == create_task)")
                .endFunc()

                //Generate Options using AI

                .build();

    }

    private static Flow createTaskBreakdownFlow() throws IOException, FlowNotCreatedException {
        User user = flowAdminApi.createUser("Dylan Scott-Dawkins");
        Flow flow = buildTaskBreakdownFlow(user);
        flowApi.createFlow(user, flow);
        return flow;
    }
}
