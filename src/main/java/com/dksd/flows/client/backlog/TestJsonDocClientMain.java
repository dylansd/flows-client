package com.dksd.flows.client.backlog;

import com.dksd.flows.client.TextReplyGenerationClientMain;
import com.dksd.flows.client.api.FlowAdminApi;
import com.dksd.flows.client.api.FlowApi;
import com.dksd.flows.common.exceptions.FlowNotCreatedException;
import com.dksd.flows.common.factories.FlowCreator;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.helper.UUIDProvider;
import com.dksd.flows.common.model.Flow;
import com.dksd.flows.common.model.User;
import com.dksd.flows.common.schema.FlowMsgData;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class TestJsonDocClientMain {

    //Uncomment this for android app
    //private static FlowApi flowApi = new FlowApi.Builder().withServerAddress("http://192.168.86.32:8080").build();
    static FlowAdminApi flowAdminApi = new FlowAdminApi.Builder().withAdminToken(TextReplyGenerationClientMain.apiAdminToken).withServerAddress("http://localhost:8080")
            .build();
    private static FlowApi flowApi = new FlowApi.Builder().withServerAddress("http://localhost:8080")
            .build();

    public static void main(String[] args) throws Exception {
        User user = flowAdminApi.createUser("Dylan Scott-Dawkins");
        Flow flow = createDietFlow(user);
        saveRecipiesToFlowDataSource(user, flow);
        List<FlowMsgData> msgs = new ArrayList<>(100);
        for (int i = 0; i < 1; i++) {
            msgs.add(flowApi.createFlowMsg(flow, JsonHelper.createObjectNode()));
        }
        long st = System.currentTimeMillis();
        flowApi.submitFlowMessages(flowApi.getUser(flow), flow.getFlowId(), msgs);
        long ed = System.currentTimeMillis();
    }

    private static Flow buildDietOptionFlow(User user) throws JsonProcessingException, FlowNotCreatedException {
        return new FlowCreator.Builder()
                .withUser(user.getUserId())
                .withName("Active inference flow")
                .withDescription("This is the base inference flow, learning/training happens in another flow")
                .beginFunc("json_infer", "https://localhost:8080/input", "post")
                .routeAll("node_selector")
                .markAsAStartStone()
                .endFunc()
                .beginFunc("node_selector", "https://localhost:8080/input", "post")
                //.withOutputField() interesting could add this here...to make it more intuitive
                .routeAll("human_ask")
                .endFunc()
                .beginFunc("human_ask", "https://localhost:8080/input", "post")
                //.routeIf("(swipe_action == down)")
                //.routeThen("select_meals")//this could be a data source actually. TODO
                //.routeIf("(swipe_action == right)")
                //.routeThen("select_meals")
                .endFunc()


                .build();
              /*
              .beginFunc("human_ask", new HumanNotifierFunction())
              .withInputTopic("selected_json")
              .withOutputTopic("human_anno")
              .routeIf("(swipe_action == down)")
              .routeThen("select_meals")
              .routeIf("(swipe_action == right)")
              .routeThen("select_meals")
              .routeAll("create_training_example")
              .endFunc()
              .beginFunc("add_training_example", new cAddTrainingExampleFunction())
              .withInputTopic("json_msg")
              .withInputTopic("human_anno")
              .withInputTopic("inference")
              .endFunc()
              .beginFunc("data_sink", new DataSinkFunction())
              .endFunc()
              .build();*/
    }

    private static Flow createDietFlow(User user) throws IOException, FlowNotCreatedException {
        Flow flow = buildDietOptionFlow(user);
        flowApi.createFlow(user, flow);
        return flow;
    }


    private static void saveRecipiesToFlowDataSource(User user, Flow flow) throws IOException {
        String recipies = "{\n" +
                "   \"meals\":[\n" +
                "      {\n" +
                "         \"recipe_id\":\"67bd9b63-f54e-409b-9afe-0bf6f9d85b23\",\n" +
                "         \"name\":\"Beef Gyros with Tahini Sauce\",\n" +
                "         \"serves\":4,\n" +
                "         \"groceries\":[\n" +
                "            {\n" +
                "               \"name\":\"nonstick cooking spray\",\n" +
                "               \"qty\":1.0,\n" +
                "               \"unit\":\"SPRAY\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"olive oil\",\n" +
                "               \"qty\":2.0,\n" +
                "               \"unit\":\"TBLSPOON\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"oregano\",\n" +
                "               \"qty\":1.0,\n" +
                "               \"unit\":\"TBLSPOON\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"garlic powder\",\n" +
                "               \"qty\":1.25,\n" +
                "               \"unit\":\"TEASPOON\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"cumin\",\n" +
                "               \"qty\":1.0,\n" +
                "               \"unit\":\"TEASPOON\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"black pepper\",\n" +
                "               \"qty\":0.5,\n" +
                "               \"unit\":\"TEASPOON\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"sea salt\",\n" +
                "               \"qty\":0.25,\n" +
                "               \"unit\":\"TEASPOON\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"beef steak\",\n" +
                "               \"qty\":1.0,\n" +
                "               \"unit\":\"POUND\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"bell pepper\",\n" +
                "               \"qty\":130.0,\n" +
                "               \"unit\":\"GRAMS\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"tahini or peanut butter\",\n" +
                "               \"qty\":2.0,\n" +
                "               \"unit\":\"TBLSPOON\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"greek yogurt\",\n" +
                "               \"qty\":0.5,\n" +
                "               \"unit\":\"CUP\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"lemon juice\",\n" +
                "               \"qty\":1.0,\n" +
                "               \"unit\":\"TBLSPOON\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"red onion\",\n" +
                "               \"qty\":1,\n" +
                "               \"unit\":\"CUP\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"pita bread\",\n" +
                "               \"qty\":200.0,\n" +
                "               \"unit\":\"GRAMS\"\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {\n" +
                "         \"recipe_id\":\"90eb98c9-b824-4a5c-804d-06136b8b140e\",\n" +
                "         \"name\":\"Brown Rice Pilay with Golden Raisins\",\n" +
                "         \"serves\":6,\n" +
                "         \"groceries\":[\n" +
                "            {\n" +
                "               \"name\":\"olive oil\",\n" +
                "               \"qty\":1.0,\n" +
                "               \"unit\":\"TBLSPOON\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"onion\",\n" +
                "               \"qty\":1.0,\n" +
                "               \"unit\":\"CUP\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"carrot\",\n" +
                "               \"qty\":0.5,\n" +
                "               \"unit\":\"CUP\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"cumin\",\n" +
                "               \"qty\":1.0,\n" +
                "               \"unit\":\"TEASPOON\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"cinnamon\",\n" +
                "               \"qty\":1.0,\n" +
                "               \"unit\":\"TEASPOON\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"brown rice\",\n" +
                "               \"qty\":2.0,\n" +
                "               \"unit\":\"CUP\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"orange\",\n" +
                "               \"qty\":1.75,\n" +
                "               \"unit\":\"CUP\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"golden raisins\",\n" +
                "               \"qty\":1.0,\n" +
                "               \"unit\":\"CUP\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"pistachios\",\n" +
                "               \"qty\":0.5,\n" +
                "               \"unit\":\"CUP\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"chives\",\n" +
                "               \"qty\":1.0,\n" +
                "               \"unit\":\"TBLSPOON\"\n" +
                "            }\n" +
                "         ]\n" +
                "      },\n" +
                "      {\n" +
                "         \"recipe_id\":\"655d13ac-08e2-45cd-aff6-81766cc6198f\",\n" +
                "         \"name\":\"Salmon Skillet Supper\",\n" +
                "         \"serves\":4,\n" +
                "         \"groceries\":[\n" +
                "            {\n" +
                "               \"name\":\"olive oil\",\n" +
                "               \"qty\":1.0,\n" +
                "               \"unit\":\"TBLSPOON\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"garlic cloves\",\n" +
                "               \"qty\":6.0,\n" +
                "               \"unit\":\"GRAMS\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"paprika\",\n" +
                "               \"qty\":1.0,\n" +
                "               \"unit\":\"TEASPOON\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"cherry tomatoes\",\n" +
                "               \"qty\":1.0,\n" +
                "               \"unit\":\"PINT\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"roasted red peppers\",\n" +
                "               \"qty\":12.0,\n" +
                "               \"unit\":\"OUNCE\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"black pepper\",\n" +
                "               \"qty\":0.25,\n" +
                "               \"unit\":\"TEASPOON\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"sea salt\",\n" +
                "               \"qty\":0.25,\n" +
                "               \"unit\":\"TEASPOON\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"salmon fillets\",\n" +
                "               \"qty\":1.0,\n" +
                "               \"unit\":\"POUND\"\n" +
                "            },\n" +
                "            {\n" +
                "               \"name\":\"lemon juice\",\n" +
                "               \"qty\":1.0,\n" +
                "               \"unit\":\"TBLSPOON\"\n" +
                "            }\n" +
                "         ]\n" +
                "      }\n" +
                "   ]\n" +
                "}";
        UUID recipiesId = UUIDProvider.provide(recipies);
        flowApi.uploadDataToFlow(user, flow, "recipes.json", recipiesId.toString(), recipies);
    }


}