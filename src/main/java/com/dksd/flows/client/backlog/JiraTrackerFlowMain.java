package com.dksd.flows.client.backlog;

import com.dksd.flows.client.TextReplyGenerationClientMain;
import com.dksd.flows.client.api.FlowAdminApi;
import com.dksd.flows.client.api.FlowApi;
import com.dksd.flows.common.exceptions.FlowNotCreatedException;
import com.dksd.flows.common.exceptions.FlowNotFoundException;
import com.dksd.flows.common.exceptions.UserNotFoundException;
import com.dksd.flows.common.factories.FlowCreator;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.model.Flow;
import com.dksd.flows.common.model.User;
import com.dksd.flows.common.schema.FlowMsgData;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple checklist for Jira tickets.
 * Is the ticket done? Where in the process am I now?
 * <p>
 * 1. Enter ticket number: {input from human}
 * 2. Create diagram of solution
 * 2. Move ticket into progress
 * 2. Create a branch
 * 3. Any UI changes needed in tandem with this ticket?
 * 3. Create unit tests/test cases
 * 4. Reach out to QA about what if nay integration tests are needed
 * 4. Test end-to-end in a local/dev environment (eg devspace)
 * 5. Merge master into branch if needed?
 * 6. Rebase branch into single commit
 * 7. Create PR
 * 8. Move ticket into "PR review"
 * 7. Merge branch into sandbox
 * 8. Test in sandbox
 * 9. Merge into integration
 * 10. Request QA testing in integration
 * 11. All conversations on PR resolved?
 * 12. All documentation updated (integration tests too)
 * 12. QA signed off?
 * 13. Move ticket into "ready for deploy"
 * 13. All test scenarios and errors covered?
 * 14. Create Entry in deploy schedule. Anybody to notify
 * 15. Merge PR in master (Except on friday)
 * 16. Deploy to live
 * 17. Verify live works post deploy
 * 18. Rollback if needed
 * 19. Move ticket into "Done"
 */
public final class JiraTrackerFlowMain {

    static FlowAdminApi flowAdminApi = new FlowAdminApi.Builder().withAdminToken(TextReplyGenerationClientMain.apiAdminToken).withServerAddress("http://localhost:8080")
            .build();
    static FlowApi flowApi = new FlowApi.Builder().withServerAddress("http://localhost:8080")
            .build();

    public static void main(String[] args) throws InterruptedException, UserNotFoundException, FlowNotFoundException, IOException, FlowNotCreatedException {
        long st = System.currentTimeMillis();
        User user = flowAdminApi.createUser("Dylan Scott-Dawkins");
        Flow flow = createTaskBreakdownFlow(user);
        flowApi.uploadDataToFlow(user, flow, "first_file.txt", "1234", "test flow data");
        System.out.println("Downloaded: " + flowApi.downloadDataFromFlow(user, flow, "first_file.txt").getData());
        //TODO testing this.
        List<FlowMsgData> msgs = new ArrayList<>(100);
        ObjectNode tnde = JsonHelper.createObjectNode();
        msgs.add(flowApi.createFlowMsg(flow, tnde));
        flowApi.submitFlowMessages(flowApi.getUser(flow), flow.getFlowId(), msgs);
        long ed = System.currentTimeMillis();
        System.out.println("Time to create user, flow + data etc: " + (ed - st));
    }

    private static Flow buildTaskBreakdownFlow(User user) throws IOException, FlowNotCreatedException {
    /* Different flows to create
    - Creating/Updating a ticket.. bug or not etc? Creating diagram if nexxessary? UI changes needed? QA etc
    * -. Any UI changes needed in tandem with this ticket?
    -. Create diagram of solution
    * -. Create unit tests/test cases
    - is this a code change? repo..?
    * -. Reach out to QA about what if nay integration tests are needed
     */

    /*
    Github only related stuff flow.
    Need a script running function which also returns the output.
    * -. backup directory
    * -. git fetch
    * -. git gst
    * -. stash any local changes
    * -. Create a branch if missing/needed
    * -. Merge master into branch if needed? (git log master..branch-X)
    * -. PUSH to remote branch only when ready (don''t let anyone see)
    * -. Merge branch into sandbox
    * -. Rebase branch into single commit (git reset branch)
    * -. Merge into integration and test in integration
    * -. Rollback if needed (note single commit and command to run)
     */

    /* - Working on ticket, from start to end, keeping it sync with everything (master, sandbox, qa etc, deploys, conflicts),
    ========================Tracking jira ticket ============================
     * -. Enter ticket number: {input from human}
     * -. Move ticket into progress
     * -. All documentation updated (integration/postman tests too)
     * -. Test end-to-end in a local/dev environment (eg devspace)
     * -. Test in devspace (aws-azure-login, devspace command etc)
     * -. Deploy and test sandbox branch (vpn needed)
     * -. Any automated/performance tests that need running?
     * -. Create PR
     * -. Move ticket into "PR review"
     * -. Request QA testing in integration
     * -. All conversations on PR resolved?
     * -. QA signed off?
     * -. Move ticket into "ready for deploy"
     * -. Create Entry in deploy schedule. Anybody to notify
     * -. Merge PR in master (Except on friday)
     * -. Deploy to live
     * -. Verify live works post deploy
     * -. monitor Rollbar and New Relic for any errors
     * -. Move ticket into "Done"*/
        return new FlowCreator.Builder()
                .withUser(user.getUserId())
                .withName("Jira track a ticket to completion")
                .withDescription("Tests the use of first class citizens")

                .beginFunc("input_ticket_number", "https://localhost:8080/input", "post")

                .routeAll("move_ticket_in_progress")
                //.routeIf("(drew == assignee AND {{ticket_number}} != 1) ")
                //.routeThen("drew")
                .endFunc()
                //What data source for this guy, should be the main pipe with the number...
                .beginFunc("move_ticket_in_progress", "https://localhost:8080/input", "post")
                .endFunc()
                .build();
    }

    private static Flow createTaskBreakdownFlow(User user) throws IOException, FlowNotCreatedException {
        Flow flow = buildTaskBreakdownFlow(user);
        flowApi.createFlow(user, flow);
        return flow;
    }

}
