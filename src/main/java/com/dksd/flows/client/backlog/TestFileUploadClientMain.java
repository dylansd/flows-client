package com.dksd.flows.client.backlog;

import com.dksd.flows.client.api.FlowApi;
import com.dksd.flows.common.client.DataObj;
import retrofit2.Response;

import java.io.File;
import java.io.IOException;

public final class TestFileUploadClientMain {

    static FlowApi flowApi = new FlowApi.Builder().withServerAddress("http://localhost:8080")
            .build();

    //Now take this and create a script that runs a specific flow...
    public static void main(String[] args) throws IOException {
        for (String arg : args) {
            System.out.println("Argument: " + arg);
        }
        Response<DataObj> response = flowApi.uploadDataToFolder(new File(args[1]));
        System.out.println(response);
    }

  /*
  Flow is to upload and download files to/from minio shared storage and list files.
  Java program should do a lot of the lifting and fun stuff
   */

}
