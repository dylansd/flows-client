package com.dksd.flows.client.backlog;

import com.dksd.flows.client.TextReplyGenerationClientMain;
import com.dksd.flows.client.api.FlowAdminApi;
import com.dksd.flows.client.api.FlowApi;
import com.dksd.flows.common.exceptions.FlowNotCreatedException;
import com.dksd.flows.common.exceptions.FlowNotFoundException;
import com.dksd.flows.common.exceptions.UserNotFoundException;
import com.dksd.flows.common.factories.FlowCreator;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.model.Flow;
import com.dksd.flows.common.model.User;
import com.dksd.flows.common.schema.FlowMsgData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class ScriptFlowMain {

    static FlowAdminApi flowAdminApi = new FlowAdminApi.Builder().withAdminToken(TextReplyGenerationClientMain.apiAdminToken).withServerAddress("http://localhost:8080")
            .build();
    static FlowApi flowApi = new FlowApi.Builder().withServerAddress("http://localhost:8080")
            .build();

    //private static Map<String, AbstractFlowMemFunction> functions = new HashMap<>();

    public static void main(String[] args) throws FlowNotCreatedException, IOException, UserNotFoundException, FlowNotFoundException {
        long st = System.currentTimeMillis();
        User user = flowAdminApi.createUser("Dylan Scott-Dawkins");
        String flowname = "Various Script Functions Flow";
        Flow flow = createFlowConfig(user, flowname).build();
        flowApi.createFlow(user, flow);

        /*File a = new File("/some/abs/path");
        File parentFolder = new File(a.getParent());
        File b = new File(parentFolder, "../some/relative/path");
        String absolute = b.getCanonicalPath(); // may throw IOException*/

        List<FlowMsgData> msgs = new ArrayList<>(1);
        for (int i = 0; i < 1; i++) {
            msgs.add(flowApi.createFlowMsg(flow, JsonHelper.createWithKeyValue("args", Arrays.toString(args))));
        }
        flowApi.submitFlowMessages(user, flow.getFlowId(), msgs);
        //Here I want to add annotations to it.
        //Problem is that the message id has changed since annotations were added to it.
        //I should fix that even if the message gets too big, thats a different problem.
        long ed = System.currentTimeMillis();
        System.out.println("Time taken: " + (ed - st));
    }

    private static FlowCreator.Builder createFlowConfig(User user, String flowName) {
        return new FlowCreator.Builder()
                .withName(flowName)
                .withUser(user)
                .withDescription("will take some command line params and decide which flow to run")

                .beginFunc("script_execution_function", "https://localhost:8080/input", "post")
                .endFunc();
    }
}
