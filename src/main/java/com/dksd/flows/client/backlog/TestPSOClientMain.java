package com.dksd.flows.client.backlog;

import com.dksd.flows.client.TextReplyGenerationClientMain;
import com.dksd.flows.client.api.FlowAdminApi;
import com.dksd.flows.client.api.FlowApi;
import com.dksd.flows.client.functions.NeuralPsoFunction;
import com.dksd.flows.common.exceptions.FlowNotCreatedException;
import com.dksd.flows.common.factories.FlowCreator;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.model.Flow;
import com.dksd.flows.common.model.User;
import com.dksd.flows.common.schema.FlowMsgData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.UUID;

/**
 * this is the full implementation aka the one big time shot.
 */
public final class TestPSOClientMain {

    //private static FlowApi flowApi = new FlowApi.Builder().withServerAddress("http://192.168.86.32:8080").build();
    static FlowAdminApi flowAdminApi = new FlowAdminApi.Builder().withAdminToken(TextReplyGenerationClientMain.apiAdminToken).withServerAddress("http://localhost:8080")
            .build();
    private static FlowApi flowApi = new FlowApi.Builder().withServerAddress("http://localhost:8080")
            .build();

    public static void main(String[] args) throws Exception {

        FlowMsgData trainMsg = createTrainMsg();
        NeuralPsoFunction psoNN = new NeuralPsoFunction();
        psoNN.handleMessage(trainMsg);

    /*Flow flow = createPsoFlow();
    List<FlowMsgData> msgs = new ArrayList<>(100);
    for (int i = 0; i < 1; i++) {
      msgs.add(flowApi.createFlowMsg(flow, createInputMsg()));
    }
    long st = System.currentTimeMillis();
    flowApi.submitFlowMessages(flowApi.getUser(flow), msgs);
    long ed = System.currentTimeMillis();
    flowApi.startFlowOnLocalEngine(flow);*/
        //TODO
    /*
    3. parquet flow from backup
4. Add queue topic name in to flow for the function step hmm... source and dest topic? think about it/
     */
    }

    private static FlowMsgData createTrainMsg() {
        ObjectNode n = JsonHelper.createObjectNode();
        n.put("train_or_predict", "train");
        ArrayNode tsamples = JsonHelper.createArrayNode();
        addTrainingSample(tsamples, new double[]{0, 0}, new double[]{1});
        addTrainingSample(tsamples, new double[]{0, 1}, new double[]{1});
        addTrainingSample(tsamples, new double[]{1, 1}, new double[]{0});
        addTrainingSample(tsamples, new double[]{1, 0}, new double[]{1});
        n.set("infer_or_training_samples", tsamples);
        ArrayNode weightArr = JsonHelper.createArrayNode();
        weightArr.add(-34.59073220294821);
        weightArr.add(6.340269110966364);
        weightArr.add(-12.355047954247143);
        weightArr.add(25.779239042847415);
        weightArr.add(64.66112992694958);
        weightArr.add(91.36287421681604);
        weightArr.add(-43.186939366856855);
        weightArr.add(-58.75957938014396);
        weightArr.add(63.370242554229854);
        weightArr.add(-84.21824256898441);
        weightArr.add(63.68147358126408);
        weightArr.add(-38.21146555565332);
        weightArr.add(5.527084682085193);
        weightArr.add(81.41222981104487);
        weightArr.add(17.145854160204507);
        n.set("weights", weightArr);

        FlowMsgData msg = new FlowMsgData();
        try {
            msg.setData(JsonHelper.toJson(n));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        msg.setId(UUID.randomUUID());
        return msg;
    }

    private static void addTrainingSample(ArrayNode tsamples, double[] inputs, double[] outputs) {
        ObjectNode newEntry = JsonHelper.createObjectNode();
        ArrayNode inputNode = JsonHelper.createArrayNode();
        ArrayNode outputNode = JsonHelper.createArrayNode();
        for (int i = 0; i < inputs.length; i++) {
            inputNode.add(inputs[i]);
        }
        for (int i = 0; i < outputs.length; i++) {
            outputNode.add(outputs[i]);
        }
        newEntry.set("inputs", inputNode);
        newEntry.set("outputs", outputNode);
        tsamples.add(newEntry);
    }

    private static void addDomainEntry(ArrayNode domains, String name, double start, double end) {
        ObjectNode newEntry = JsonHelper.createObjectNode();
        newEntry.put("name", name);
        newEntry.put("start", start);
        newEntry.put("end", end);
        domains.add(newEntry);
    }

    private static Flow buildDietOptionFlow(User user) throws FlowNotCreatedException {
        return new FlowCreator.Builder()
                .withUser(user.getUserId())
                .withName("Diet Options flow")
                .withDescription("Diet options flow")
                .beginFunc("create_options", "https://localhost:8080/input", "post")
                .routeAll("human_ask")
                .endFunc()
                .beginFunc("human_ask", "https://localhost:8080/input", "post")
                //.routeIf("(swipe_action == down)")//try again.
                //.routeThen("create_options")
                .endFunc()
                .build();
    }

    private static Flow createPsoFlow() throws IOException, FlowNotCreatedException {
        User user = flowAdminApi.createUser("Dylan Scott-Dawkins");
        Flow flow = buildDietOptionFlow(user);
        flowApi.createFlow(user, flow);
        return flow;
    }

    private static JsonNode createInputMsg() throws IOException {
        return JsonHelper.createWithKeyValue("msg", "not important");
        //hmm juicy. or tasty
        //Your API key for dylansd@gmail.com is:
        //
        //LhvSbitQmESzol67lPbWRTocw5VwpiYMHLz39gJd
        //example:
        //https://api.nal.usda.gov/fdc/v1/foods/search?query=apple&pageSize=2&api_key=LhvSbitQmESzol67lPbWRTocw5VwpiYMHLz39gJd
        //Were looking for a good spread of nutrients....
        //and alternatives.
        //Start small:
        //What meal to have for the next day?

    }


}