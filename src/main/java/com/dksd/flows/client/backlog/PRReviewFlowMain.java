package com.dksd.flows.client.backlog;

import com.dksd.flows.client.TextReplyGenerationClientMain;
import com.dksd.flows.client.api.FlowAdminApi;
import com.dksd.flows.client.api.FlowApi;
import com.dksd.flows.common.exceptions.FlowNotCreatedException;
import com.dksd.flows.common.exceptions.UserNotFoundException;
import com.dksd.flows.common.factories.FlowCreator;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.model.Flow;
import com.dksd.flows.common.model.User;
import com.dksd.flows.common.schema.FlowMsgData;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Simple checklist for Jira tickets.
 * Is the ticket done? Where in the process am I now?
 * <p>
 * 1. move ticket into progress
 * 2. create a branch
 * 3. Create unit tests
 * 4. Test end-to-end in devspace
 * 5. Merge master into branch if needed?
 * 6. Rebase branch into single commit
 * 7. Merge into sandbox
 * 8. Test in sandbox
 * 9. Merge into integration
 * 10. Request QA testing in integration
 * 11.
 */
public final class PRReviewFlowMain {

    static FlowAdminApi flowAdminApi = new FlowAdminApi.Builder().withAdminToken(TextReplyGenerationClientMain.apiAdminToken).withServerAddress("http://localhost:8080")
            .build();
    static FlowApi flowApi = new FlowApi.Builder().withServerAddress("http://localhost:8080")
            .build();

    public static void main(String[] args) throws IOException, UserNotFoundException, FlowNotCreatedException {
        Flow flow = createTaskBreakdownFlow();
        List<FlowMsgData> msgs = new ArrayList<>(100);
        for (int i = 0; i < 1; i++) {
            msgs.add(flowApi.createFlowMsg(flow, getTaskNode()));
        }
        long st = System.currentTimeMillis();
        flowApi.submitFlowMessages(flowApi.getUser(flow), flow.getFlowId(), msgs);
        long ed = System.currentTimeMillis();

    }

    /*
    What do I want here...
    1. {office finish, est_mins 10*24*60}
    if The above is too high then split it up and send to human with three options...
    2. {office finish, est_mins 10*24*60}, {office outside window trim, est_mins 2*24*60}, {office inside siding finish, est_mins 2*24*60},
       {office outside painting finish, est_mins 2*24*60}
    3. Human updates/ annotations presented in 2. Swipes left or right etc.
    4. Filter the resulting annotations and send back to step 2.
     */
    private static Flow buildTaskBreakdownFlow(User user) throws IOException, FlowNotCreatedException {
    /*
    Scenario:
    Connect to Github for a sample project and get the PR open.

    */
        return new FlowCreator.Builder()
                .withUser(user.getUserId())
                .withName("Task breakdown flow")
                .withDescription("There is a task defined, with a size and iteration count, " +
                        "we break down the task by adding a child task with a smaller est min and higher count." +
                        "At the end of the day, the iteration * time should be constant down the graph, estimating parents higher")
                .beginFunc("pass_thru", "https://localhost:8080/input", "post")
                //.routeIf("(est_min > 5 AND completed == false)")
                //.routeThen("taskSplit")
                .markAsAStartStone()
                .endFunc()
                .beginFunc("taskSplit", "https://localhost:8080/input", "post")
                .routeAll("human_ask")
                .endFunc()
                .beginFunc("human_ask", "https://localhost:8080/input", "post")
                //.routeIf("(swipe_action == up)")
                //.routeThen("pass_thru")
                .endFunc()
                .build();
    }

    private static Flow createTaskBreakdownFlow() throws IOException, FlowNotCreatedException {
        User user = flowAdminApi.createUser("Dylan Scott-Dawkins");
        Flow flow = buildTaskBreakdownFlow(user);
        flowApi.createFlow(user, flow);
        return flow;
    }

    /**
     * Submit will automagically save this and get an id if needed.
     *
     * @return
     */
    public static JsonNode getTaskNode() {
        ObjectNode tnde = JsonHelper.createObjectNode();
        tnde.put("task_name", "Office Completion");
        tnde.put("task_description", "Finish the office, paint steps inside, clean up etc");

        //Do  want
        //ArrayNode arrNode = JsonHelper.createArrayNode();
        //we want vertexs and edges...
        //Create array

        //tnde.put("prerequisites", );
        return tnde;
    }
}
