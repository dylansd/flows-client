package com.dksd.flows.client.api;

import com.dksd.flows.common.client.ServiceGenerator;
import com.dksd.flows.common.client.UserClient;
import com.dksd.flows.common.model.User;
import okhttp3.MediaType;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

public final class FlowAdminApi {

    public static final MediaType PLAIN = MediaType.get("application/plain; charset=utf-8");
    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    public static final MediaType BINARY = MediaType.get("application/octet-stream; charset=utf-8");

    private final String adminApiToken;
    private final UserClient userClient;

    public FlowAdminApi(String adminApiToken, UserClient userClient) {
        this.adminApiToken = adminApiToken;
        this.userClient = userClient;
    }

    public Optional<User> getUser(UUID userId) throws IOException {
        Objects.requireNonNull(userId);
        return Optional.ofNullable(userClient.getUser(userId).execute().body());
    }

    public User createUser(String userName) throws IOException {
        Objects.requireNonNull(userName);
        User nUser = new User(userName);
        nUser.setCashPaidInCents(40000000);
        return userClient.putUser(nUser).execute().body();
    }

    public static class Builder {
        private String address;
        private String adminToken;

        public Builder withServerAddress(String address) {
            this.address = address;
            return this;
        }

        public Builder withAdminToken(String adminToken) {
            this.adminToken = adminToken;
            return this;
        }

        public FlowAdminApi build() {
            UserClient userClient = ServiceGenerator.createService(address, UserClient.class, adminToken);
            return new FlowAdminApi(adminToken, userClient);
        }
    }

}
