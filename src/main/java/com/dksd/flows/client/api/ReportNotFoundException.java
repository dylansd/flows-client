package com.dksd.flows.client.api;

import java.util.UUID;

public class ReportNotFoundException extends Exception {
    public ReportNotFoundException(UUID msgId) {
        super(msgId.toString());
    }
}
