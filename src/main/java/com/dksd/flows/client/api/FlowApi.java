package com.dksd.flows.client.api;

import com.dksd.flows.client.config.FlowEngineConfig;
import com.dksd.flows.common.client.*;
import com.dksd.flows.common.exceptions.FlowNotCreatedException;
import com.dksd.flows.common.exceptions.FlowNotFoundException;
import com.dksd.flows.common.exceptions.UserNotFoundException;
import com.dksd.flows.common.factories.FlowCreator;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.helper.UUIDProvider;
import com.dksd.flows.common.model.Flow;
import com.dksd.flows.common.model.FlowId;
import com.dksd.flows.common.model.ReportPath;
import com.dksd.flows.common.model.User;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;
import com.fasterxml.jackson.databind.JsonNode;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import org.apache.http.HttpStatus;
import retrofit2.Response;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.function.Supplier;

public final class FlowApi {

    public static final MediaType PLAIN = MediaType.get("application/plain; charset=utf-8");
    public static final MediaType JSON = MediaType.get("application/json; charset=utf-8");
    public static final MediaType BINARY = MediaType.get("application/octet-stream; charset=utf-8");

    private final UserClient userClient;
    private final FlowClient flowClient;
    private final DataClient dataClient;
    private final FlowMessageClient flowMessageClient;

    private final ReportClient reportClient;
    private final AnnotationClient annotationClient;
    private final FlowEngineConfig flowEngineConfig;

    public FlowApi(UserClient userClient,
                   FlowClient flowClient,
                   DataClient dataClient,
                   AnnotationClient annotationClient,
                   FlowMessageClient flowMessageClient, ReportClient reportClient) {
        this.userClient = userClient;
        this.flowClient = flowClient;
        this.dataClient = dataClient;
        this.flowMessageClient = flowMessageClient;
        this.annotationClient = annotationClient;
        this.reportClient = reportClient;
        this.flowEngineConfig = new FlowEngineConfig(
                flowClient,
                dataClient,
                annotationClient,
                flowMessageClient);
    }

    /**
     * Stores it on the storage medium/service provider.
     * The load functions will read it from there.
     *
     * @param flowMessages flow messages to store
     */
    public List<FlowMsgData> submitFlowMessages(User user, FlowId flowId, Collection<FlowMsgData> flowMessages) {
        try {
            return flowMessageClient.putFlowMessages(user.getUserId(),
                    flowId,
                    flowMessages).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }
                /*enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {

                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                System.err.println("Failed to put messgaes");
            }
        });*/
        return Collections.emptyList();
    }

    public void submitAnnotation(User user, FlowId flowId, UUID origMsgId, List<Annotation> annotations) {
        try {
            annotationClient.putAnnotations(user.getUserId(),
                    flowId,
                    origMsgId,
                    annotations).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String auth(String apiKey) {
        return "Token token=" + apiKey;
    }

    public void uploadDataToFlow(User user,
                                 Flow flow,
                                 String filename,
                                 String oid,
                                 String data) throws IOException {
        Objects.requireNonNull(user.getUserId());
        dataClient.putObject(new DataUploadObj(
                filename,
                oid,
                user.getUserId().getUserId(),
                flow.getFlowId().getFlowId(),
                auth(user.getApiKey()),
                data)).execute().body();
    }

    /* Can be used to upload data to any folder. */
    public Response<DataObj> uploadDataToFolder(File file) throws IOException {
        //MultipartBody.Part filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse("plain/*"), file));
        RequestBody fbody = RequestBody.create(MediaType.parse("image/*"),
                file);
        RequestBody filename = RequestBody.create(MediaType.parse("plain/*"),
                file.getName());
        return dataClient.uploadFile(fbody, filename).execute();
    }

    public DataObj downloadDataFromFlow(
            User user,
            Flow flow,
            String filename) throws IOException {
        Objects.requireNonNull(user.getUserId());
        return dataClient.getObject(
                filename,
                user.getUserId().getUserId(),
                flow.getFlowId().getFlowId()).execute().body();
    }

    public Flow getOrCreate(String userName, FlowCreator.Builder flowBuilder) throws UserNotFoundException, FlowNotCreatedException {
        User user = getUser(userName);
        return flowBuilder.withUser(user).build();
    }

    public Flow getOrCreateFlow(User user, String flowName, Supplier<Flow> flowSupplier) throws FlowNotCreatedException {
        Flow flow = null;
        try {
            flow = getFlow(user, UUIDProvider.provide(flowName));
        } catch (FlowNotFoundException e) {
            try {
                Flow createFlow = flowSupplier.get();
                createFlow.setUserId(user.getUserId());
                return createFlow(user, createFlow);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return flow;
    }

    /**
     * This is better, but needs some work to finish TODO
     */
    public FlowMsgData createFlowMsg(Flow flow, JsonNode msg) {
        Objects.requireNonNull(flow);
        Objects.requireNonNull(msg);
        try {
            FlowMsgData fm = new FlowMsgData();
            fm.setData(JsonHelper.toJson(msg));
            fm.setId(UUID.randomUUID());
            return fm;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public User getUser(Flow flow) throws UserNotFoundException {
        Objects.requireNonNull(flow.getUserId().getUserId());
        User user = null;
        try {
            user = userClient.getUser(flow.getUserId().getUserId()).execute().body();
        } catch (IOException e) {
            throw new UserNotFoundException("User not found: " + flow.getUserId().getUserId(), e);
        }
        return user;
    }

    public User getUser(String userName) throws UserNotFoundException {
        UUID userId = UUIDProvider.provide(userName);
        return getUser(userId);
    }

    public User getUser(UUID userId) throws UserNotFoundException {
        Objects.requireNonNull(userId);
        try {
            return userClient.getUser(userId).execute().body();
        } catch (IOException e) {
            throw new UserNotFoundException("User not found: " + userId.toString(), e);
        }
    }

    public Flow getFlow(User user, UUID flowId) throws FlowNotFoundException {
        Objects.requireNonNull(flowId);
        Response<Flow> result = null;
        try {
            result = flowClient.getFlow(flowId, user.getUserId().getUserId()).execute();
        } catch (IOException e) {
            e.printStackTrace();
            throw new FlowNotFoundException("Flow not found: " + flowId + " for user id: " + user.getUserId());
        }
        if (result.code() == HttpStatus.SC_NOT_FOUND) {
            throw new FlowNotFoundException("Flow not found: " + flowId + " for user id: " + user.getUserId());
        }
        return result.body();
    }

    public ReportPath getReport(User user, UUID msgId) throws ReportNotFoundException {
        Objects.requireNonNull(msgId);
        try {
            return reportClient.getReport(user.getUserId().getUserId(), msgId).execute().body();
        } catch (Exception e) {
            e.printStackTrace();
            throw new ReportNotFoundException(msgId);
        }
    }

    public Flow createFlow(User user, Flow flow) throws IOException, FlowNotCreatedException {
        Objects.requireNonNull(flow);
        Response<Flow> result = flowClient.putFlow(user.getUserId().getUserId(), flow).execute();
        if (!result.isSuccessful()) {
            throw new FlowNotCreatedException("Could not create flow: " + result.toString());
        }
        return result.body();
    }

    public static class Builder {
        private String address;
        private String authToken;

        public Builder withServerAddress(String address) {
            this.address = address;
            return this;
        }

        public Builder withAuthToken(String authToken) {
            this.authToken = authToken;
            return this;
        }

        public FlowApi build() {
            UserClient userClient = ServiceGenerator.createService(address, UserClient.class, authToken);
            FlowClient flowClient = ServiceGenerator.createService(address, FlowClient.class, authToken);
            DataClient dataClient = ServiceGenerator.createService(address, DataClient.class, authToken);
            AnnotationClient annotationClient = ServiceGenerator.createService(address, AnnotationClient.class, authToken);
            FlowMessageClient flowMessageClient = ServiceGenerator.createService(address, FlowMessageClient.class, authToken);
            ReportClient reportClient = ServiceGenerator.createService(address, ReportClient.class, authToken);
            return new FlowApi(userClient, flowClient, dataClient, annotationClient, flowMessageClient, reportClient);
        }
    }

}
